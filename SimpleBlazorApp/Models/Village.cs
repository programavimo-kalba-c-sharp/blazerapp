﻿using SimpleBlazorApp.DataAccess;

namespace SimpleBlazorApp.Models
{
    public class Village : Settlement
    {
        public Village(string name, double area, int population)
        {
            Name = name;
            Type = SettlementType.Village;
            Area = area;
            Population = population;
        }
    }
}
