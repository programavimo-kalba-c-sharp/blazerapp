﻿using SimpleBlazorApp.DataAccess;

namespace SimpleBlazorApp.Models
{
    public class Town : Settlement
    {
        public Town(string name, double area, int population)
        {
            Name = name;
            Type = SettlementType.Town;
            Area = area;
            Population = population;
        }
    }
}
