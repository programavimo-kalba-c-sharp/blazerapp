﻿using SimpleBlazorApp.DataAccess;

namespace SimpleBlazorApp.Models
{
    public class City : Settlement
    {
        public City(string name, double area, int population)
        {
            Name = name;
            Type = SettlementType.City;
            Area = area;
            Population = population;
        }
    }
}
