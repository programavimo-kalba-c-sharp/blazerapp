﻿namespace SimpleBlazorApp.DataAccess {
    public interface ISettlement {
        string Id { get; }
        string Name { get; }
        SettlementType Type { get; }
        double Area { get; }
        int Population { get; }
    }
}
