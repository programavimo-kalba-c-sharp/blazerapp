﻿using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using System.Xml.Linq;

namespace SimpleBlazorApp.DataAccess
{
    public enum SettlementType
    {
		[Display(Name = "Village", ResourceType = typeof(string))]
		Village,
		[Display(Name = "Town", ResourceType = typeof(string))]
		Town,
		[Display(Name = "City", ResourceType = typeof(string))]
		City,
    }
}
