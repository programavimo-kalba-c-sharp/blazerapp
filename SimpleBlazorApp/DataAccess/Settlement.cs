﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SimpleBlazorApp.DataAccess
{
    public class Settlement
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
        public string Name { get; set; }
        public SettlementType Type { get; set; } = SettlementType.Village;
        public double Area { get; set; }
        public int Population { get; set; }
    }
}
