﻿using SimpleBlazorApp.DataAccess;

namespace SimpleBlazorApp.Controller {
	public interface ISettlementService {
		void SaveOrUpdate(Settlement settlement);
		Settlement GetSettlement(string id);
		List<Settlement> GetSettlements();
		string Delete(string id);
	}
}
