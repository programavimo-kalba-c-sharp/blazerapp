﻿using MongoDB.Driver;
using SimpleBlazorApp.DataAccess;

namespace SimpleBlazorApp.Controller
{
	public class SettlementService : ISettlementService {
		private MongoClient _client = null;
		private IMongoDatabase _database = null;
		private IMongoCollection<Settlement> _settlements = null;
		public SettlementService() {
			_client = new MongoClient("mongodb://localhost:27017");
			_database = _client.GetDatabase("SettlementsDB");
			_settlements = _database.GetCollection<Settlement>("Settlements");
		}

		public string Delete(string id) {
			_settlements.DeleteOne(x => x.Id == id);
			return "Deleted";
		}

		public Settlement GetSettlement(string id) {
			return _settlements.Find(x => x.Id == id).FirstOrDefault();
		}

		public List<Settlement> GetSettlements() {
			return _settlements.Find(FilterDefinition<Settlement>.Empty).ToList();
		}

		public void SaveOrUpdate(Settlement settlement) {
			var settlementObj = _settlements.Find(x => x.Id == settlement.Id).FirstOrDefault();
			if (settlementObj == null)
				_settlements.InsertOne(settlement);
			else
				_settlements.ReplaceOne(x => x.Id == settlement.Id, settlement);
		}
	}
}
